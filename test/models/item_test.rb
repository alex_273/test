require 'test_helper'

class ItemTest < ActiveSupport::TestCase
  def setup
    @item = Item.create name: "Flower1", description: "....."
  end 

  test "should fail validate" do
    item1 = Item.new
    assert_not item1.validate
    item1.name = "Flower1"
    item1.description = "ff"
    assert_not item1.validate
    assert_not item1.save
  end

  test "should create" do 
    item1 = Item.new name: "www", description: "dd"
    assert item1.validate
    assert item1.save
  end

end
