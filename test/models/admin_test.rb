require 'test_helper'

class AdminTest < ActiveSupport::TestCase

  def setup
    @admin = Admin.create(password: "admin", password_confirmation: "admin")
    @admin_fixture = admins(:admin)
  end

  test "should authenticate" do
    assert @admin.authenticate("admin")
    assert @admin_fixture.authenticate("admin")
  end

  test "should not authenticate" do 
    assert_not @admin.authenticate("wrong")
    assert_not @admin_fixture.authenticate("wrong")
  end
end
