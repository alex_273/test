require 'test_helper'

class GuestTest < ActiveSupport::TestCase
  def setup
    @guest_1 = Guest.create
    @guest_2 = guests(:guest_2)
  end

  test "should authenticate" do
    assert @guest_1.authenticated?(@guest_1.guest_token)
    assert @guest_2.authenticated?("guest_2")
  end

  test "should not authenticate" do
    assert_not @guest_1.authenticated?("wrong_token")
    assert_not @guest_2.authenticated?("wrong_pass")
  end
end
