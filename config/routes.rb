Rails.application.routes.draw do
  root 'guest_pages#home'
  get '/admin', to: 'admin_pages#home'
  get '/income', to: 'admin_pages#income'
  get '/settings', to: 'admin_pages#settings'
  get '/guest', to: 'sessions_for_guests#create', as: 'new_guest'
  get '/guest_orders', to: 'orders#guest_orders', as: 'guest_orders'
  get '/see_order', to: 'orders#see_order', as: 'see_order'
  get '/cart', to: 'orders#cart', as: 'cart'
  get '/see_order_admin', to: 'orders#see_order_admin', as: 'see_order_admin'
  get '/login', to: 'session_for_admin#new', as: 'login'
  post '/login', to: 'session_for_admin#create'
  resources :items
  resources :orders
  resources :ordered_items
end
