class CreateOrders < ActiveRecord::Migration[5.2]
  def change
    create_table :orders do |t|
      t.integer :guest_id
      t.text :description
      t.boolean :cart, :default => true
      t.boolean :checkout, :default => false
      t.boolean :pending, :default =>  false
      t.boolean :canceled, :default => false
      t.boolean :delivered, :default => false

      t.timestamps
    end
  end
end
