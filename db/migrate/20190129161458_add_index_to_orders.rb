class AddIndexToOrders < ActiveRecord::Migration[5.2]
  def change
    add_index :orders, :pending
    add_index :orders, :delivered
  end
end
