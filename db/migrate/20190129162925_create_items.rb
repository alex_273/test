class CreateItems < ActiveRecord::Migration[5.2]
  def change
    create_table :items do |t|
      t.string :name
      t.text :description
      t.decimal :price
      t.integer :sold
      t.boolean :available, default: false

      t.timestamps
    end
  end
end
