# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Admin.create!(name: "admin", password: "admin", password_confirmation: "admin")

5.times do |n|
  guest = Guest.create!
  order1 = guest.orders.create! description: "cart", cart: true
  order2 = guest.orders.create! description: "checkout", checkout:true, cart: false
  order3 = guest.orders.create! description: "pending", cart: false, pending: true
  order4 = guest.orders.create! description: "delivered", cart: false, delivered: true
  order5 = guest.orders.create! description: "canceled", cart: false, canceled: true
  4.times do |m|
    num = Faker::IDNumber.invalid
    name = "Flower_#{num}"
    description = "This is flower Flower_#{num}"
    price = 1.5
    item = Item.create! name: name, description: description, price: price, sold: 0, available: true
      OrderedItem.create! order_id: order1.id, item_id: item.id, quantity: n*m, amount_of_money: m*n*item.price
      OrderedItem.create! order_id: order2.id, item_id: item.id, quantity: n*m, amount_of_money: m*n*item.price

      OrderedItem.create! order_id: order3.id, item_id: item.id, quantity: n*m, amount_of_money: m*n*item.price

      OrderedItem.create! order_id: order4.id, item_id: item.id, quantity: n*m, amount_of_money: m*n*item.price

      OrderedItem.create! order_id: order5.id, item_id: item.id, quantity: n*m, amount_of_money: m*n*item.price

  end
end
