module SessionsForGuestsHelper
  def log_in_guest(guest)
    session[:guest_id] = guest.id
    cookies.permanent.signed[:guest_id] = guest.id
    cookies.permanent[:guest_token] = guest.guest_token
  end

  def current_guest
    if (guest_id = session[:guest_id])
      @current_guest ||= Guest.find_by(id: guest_id)
    elsif (guest_id = cookies.signed[:guest_id])
      guest = Guest.find_by(id: guest_id)
      if guest && guest.authenticated?(cookies[:guest_token])
        log_in_guest guest
        @current_guest = guest
      end
    end
  end

  def logged_in_guest?
    !current_guest.nil?
  end

  def log_in_admin(admin)
    session[:admin_id] = admin.id
  end
  
  def current_admin
    if (admin_id = session[:admin_id])
      @current_admin ||= Admin.find_by(id: admin_id)
    end
  end

  def is_admin?
    !current_admin.nil?
  end
end
