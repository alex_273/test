class SessionsForGuestsController < ApplicationController

  def create
    guest = Guest.create
    log_in_guest guest
    redirect_to root_path
  end

end
