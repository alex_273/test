class ItemsController < ApplicationController
before_action :admin

  def index
    @items = Item.all.paginate(page: params[:page], per_page: 7)
  end
 
  def new
    @item = Item.new
  end

  def create
    @item = Item.new(item_params)
    if @item.save
      redirect_to items_path
    else
      render 'new'
    end
  end

  def edit
    @item = Item.find(params[:id])
  end

  def update
    @item = Item.find(params[:id])
    if @item.update_attributes(item_params)
      redirect_to items_path
    else
      render 'edit'
    end
  end

  def destroy
    Item.find(params[:id]).destroy
    redirect_to items_path
  end

  private
    def admin
      redirect_to root_path unless is_admin?
    end

    def item_params
      params.require(:item).permit(:name, :description, :price)
    end
end
