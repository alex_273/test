class OrderedItemsController < ApplicationController
before_action :admin, only: [:index]
  def index
    @ordered_item = OrderedItem.new
    if params[:order_id].nil?
      @ordered_items = OrderedItem.all
    else
      @ordered_items = OrderedItem.where("order_id = ?", params[:order_id])
    end
  end

  def new
    @ordered_item = OrderedItem.new
    @item_id = params[:item_id]
  end

  def edit
    @ordered_item = OrderedItem.find(params[:id])
  end

  def update
    @ordered_item = OrderedItem.find(params[:id])
    if @ordered_item.update_attributes(ordered_item_params)
      redirect_to  root_path
    else
      render 'edit'
    end
  end

  def destroy
    order = OrderedItem.find(params[:id]).order
    OrderedItem.find(params[:id]).destroy
    ordered_items = order.ordered_items
    if ordered_items.count == 0
      order.destroy
    end
    redirect_to root_path
  end

  def create
    if logged_in_guest?
      cart = false
      Order.where("guest_id = ?", current_guest.id).each do |n|
        if n.cart
          cart = true
          @ordered_item = OrderedItem.new(ordered_item_params)
          @ordered_item.order_id = n.id
          @ordered_item.item_id = params[:item_id]
          @ordered_item.amount_of_money = Item.find(params[:item_id]).price*@ordered_item.quantity
          if @ordered_item.save
            redirect_to cart_path
          else
            redirect_to new_ordered_item_path(item_id: params[:item_id])
          end
        end
      end
      if cart == false
        order = @current_guest.orders.create description: "cart", cart: true
        @ordered_item = OrderedItem.new(ordered_item_params)
        @ordered_item.order_id = order.id
        @ordered_item.item_id = params[:item_id]
        @ordered_item.amount_of_money = Item.find(params[:item_id]).price*@ordered_item.quantity
        if @ordered_item.save
          redirect_to cart_path
        else
          redirect_to new_ordered_item_path(item_id: params[:item_id])
        end
      end
    else
      Guest.create
    end
  end

  private
    def admin
      redirect_to root_path unless is_admin?
    end

    def ordered_item_params
      params.require(:ordered_item).permit(:quantity)
    end
end
