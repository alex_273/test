class OrdersController < ApplicationController
before_action :admin, only: [:index, :see_order_admin, :edit]

  def index
    if (filter = params[:filter])
      @orders = Order.where("#{filter}" => true).paginate(page: params[:page], per_page: 7)
    else
      @orders = Order.all.paginate(page: params[:page], per_page: 7)
    end
  end

  def guest_orders
    if (filter = params[:filter])
      @orders = current_guest.orders.where("#{filter}" => true).paginate(page: params[:page], per_page: 7)
    else
      @orders = current_guest.orders.paginate(page: params[:page], per_page: 7)
    end
  end

  def see_order
    @order = current_guest.orders.where(id: params[:id])
    if !@order[0].nil?
      @ordered_items = @order[0].ordered_items
    end
    if !@ordered_items.nil?
      @items = @ordered_items.map { |n| n.item.name }
    end
  end

  def see_order_admin
    @order = Order.find(params[:id])
    @ordered_items = @order.ordered_items
    if !@ordered_items.nil?
      @items = @ordered_items.map { |n| n.item.name }
    end
  end

  def cart
    @cart_order = current_guest.orders.where(cart: true)
    if !@cart_order[0].nil?
      @ordered_items = @cart_order[0].ordered_items
    end
    if !@ordered_items.nil?
      @items = @ordered_items.map { |n| n.item.name }
    end
  end

  def edit
    @order = Order.find(params[:id])
  end

  def update
    @order = Order.find(params[:id])
    if params[:order1][:state] == "canceled"
      @order.canceled = true
      @order.description = "canceled"
    else
      @order.canceled = false
    end

    if params[:order1][:state] == "pending"
      @order.pending = true
      @order.description = "pending"
    else
      @order.pending = false
    end
    
    if params[:order1][:state] == "cart"
      @order.cart = true
      @order.description = "cart"
    else
      @order.cart = false
    end

    if params[:order1][:state] == "checkout"
      @order.checkout = true
      @order.description = "checkout"
    else
      @order.checkout = false
    end

    if params[:order1][:state] == "delivered"
      @order.delivered = true
      @order.description = "delivered"
    else
      @order.delivered = false
    end
    
    @order.save
    if !current_admin.nil?
      redirect_to orders_path
    elsif !current_guest.nil?
      redirect_to guest_orders_path
    else
      redirect_to root_path
    end
  end

  private

    def admin
      redirect_to root_path unless is_admin?
    end 

    def order_params
      params.require(:order1).permit(:cart, :checkout, :pending, :canceled, :delivered)
    end
end
