class GuestPagesController < ApplicationController
before_action :logged_in_guest

  def home
    @items = Item.all.paginate(page: params[:page], per_page: 12)
  end

  private

    def logged_in_guest
      redirect_to new_guest_path unless logged_in_guest?
    end
end
