class SessionForAdminController < ApplicationController

  def new
  end

  def create
    @admin = Admin.find_by(name: params[:session][:name])
    if @admin && @admin.authenticate(params[:session][:password])
      log_in_admin @admin
      redirect_to '/admin'
    else
      render 'new'
    end
  end

end
