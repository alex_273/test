class AdminPagesController < ApplicationController
before_action :admin 

  def home
    @pending = Order.where("pending = ?", true).count
    @delivered = Order.where("delivered = ?", true).count
    @available = Item.where("available = ?", true).count
    @unavailable = Item.where("available = ?", false).count
    @total = 0
    Item.all.each do |n|
      if !n.sold.nil? && !n.price.nil?
        @total += n.sold*n.price
      end
    end
  end

  private

    def admin
      redirect_to root_path unless is_admin?
    end
end
