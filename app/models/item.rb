class Item < ApplicationRecord
  has_many :ordered_items
  validates :name, presence: true, length: { minimum: 2 }, uniqueness: true
  validates :description, presence: true, length: { maximum: 50 }
end
