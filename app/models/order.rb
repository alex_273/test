class Order < ApplicationRecord
  belongs_to :guest
  has_many :ordered_items
end
