class Guest < ApplicationRecord
  has_many :orders, dependent: :destroy
  attr_accessor :guest_token
  before_save :remember_for_session

  def Guest.digest(string)
    cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST : BCrypt::Engine.cost
          BCrypt::Password.create(string, cost: cost)
  end

  def Guest.new_token
    SecureRandom.urlsafe_base64
  end

  def remember_for_session
    self.guest_token = Guest.new_token
    self.guest_digest = Guest.digest(self.guest_token)
  end
 
  def authenticated?(unencrypted_pass)
    BCrypt::Password.new(guest_digest) == unencrypted_pass
  end 
end
